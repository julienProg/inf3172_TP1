/*******************************************************************************
 
 Arrrchiveur 
 
  le program permet d'archiver ce qu'on trouve dans le dossier passer en parametre.
 
  SYNOPSIS         : arrr [options] <arrrchive> chemin
   
  ARGUMENT         : arrr stocke les fichiers et répertoires d'une sous-arborescence 
					 dans un fichier d'arrrchive.

					 arrrchive est le nom du fichier d'arrrchive à créer. 
					 Si celui-ci existe déjà, il est écrasé et remplacé par 
					 un nouveau contenu.

  					 L’extension traditionnelle des fichiers d'arrrchive est .arrr.

					 chemin est le chemin d'un répertoire à archiver. 
					 Celui-ci est parcouru récursivement.
 
  FONCTIONNEMENT   : Le programme lit dans le repertoire specifier et archive les 
					 informations d'une facon precise
  
  ERREUR           : Les options envoye n'existe pas ou sont incorrecte.
                     Nombre de paramètres incorrecte
					 Le ficher d'archive ne peut etre creer
					 Le reportoire n'est pas trouver ou non accessible.
 
  AUTEUR           : Juien Perron PERJ23069309
					 Jean-Christophe Décary DECJ20119200
 
 
*******************************************************************************/
# define SEEK_DATA      3       /* Seek to next data.  */
# define SEEK_HOLE      4       /* Seek to next hole.  */

#include<stdio.h>
#include<stdlib.h>
#include <stdbool.h>
#include<string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

//Structure qui contient le numero N-iode et son path
struct iNode{
	long int numBerInode;
	char * path;
};

//Structure qui permet de faire un tableau dynamique qui contient <iNode>.
struct tableDeH {
	int nbElementTotal;
	int nbElement;
	struct iNode ** tableauINode;
};

/*
* Gestion erreur de write
* 
* @param fichier Le fichier ou le string doit etre ecrit.
* @param unString Le string qui va etre ecrit dans le fichier.
* 
* @return 0 si tout se passe bien et -1 si erreur.
*
*/
int ecrire (int file, char * texte){
	if(write(file, texte, strlen(texte)) == -1){
		perror("Erreur lors de l'ecriture dans le fichier");
		return -1;
	}
	return 0;
}


/*
* Permet de convertir un nombre en string
* 
* @param unNombre Le nombre a convertire.
* @param resultat La chaine de caractere de ce nombre.
* 
* @return la chaine de caractere du nombre passe en parametre.
*
*/
char * convertIntAString (long unNombre, char * resultat){
	
	 char * valeurPossible = "0123456789";
	
	 int nbInt = 0;
	 long unNombreTmp = unNombre;
	 
	 do{
		 nbInt++;
		 unNombreTmp = unNombreTmp/10;
	 }while (unNombreTmp);
		 
	 int i;
	 for(i = nbInt-1; i >= 0; i--){
		resultat[i] = valeurPossible[unNombre%10];
		 unNombre = unNombre/10;
	 }
	 resultat[nbInt] = '\0';

	 return resultat;
}

/*
* Insere le iNode et le path d'un fichier HardLink dans le tableau. 
* Si le tableau est complet, celui est copier dans un plus grand tableau
* 
* @param iNode L'id du fichier a trouver dans le tableau.
* @param pathInode Le chemin vers le fichier envoyer.
* @param tab Le tableau qui contient tout les ficher hardLink archivier (ceux qui on plus de 2).
*
*/
void insererINode (long int iNode, char * pathInode, struct tableDeH *tab){
	
	//Creer un plus gros tableau de n*2
	if(tab->nbElement >= tab->nbElementTotal){
		int tailleNewTableau = tab->nbElementTotal * 2;
			
		struct tableDeH *tabTmp = malloc(sizeof(int)+sizeof(tailleNewTableau*sizeof(long int)+sizeof(char*)));
		
		tabTmp->nbElementTotal = tailleNewTableau;
		tabTmp->nbElement = tab->nbElement;
		tabTmp->tableauINode = malloc(tailleNewTableau*sizeof(long int)+sizeof(char*));
		
		int i;
		for(i = 0; i < tailleNewTableau; i++){
			tabTmp->tableauINode[i] = malloc(sizeof(long int)+sizeof(char*));
			
			if(tab->nbElement > i){
				tabTmp->tableauINode[i]->numBerInode = tab->tableauINode[i]->numBerInode;						
				tabTmp->tableauINode[i]->path = tab->tableauINode[i]->path;
			}else{
				tabTmp->tableauINode[i]->numBerInode = 0;						
				tabTmp->tableauINode[i]->path = NULL;
			}

		}
		
		tab = tabTmp;
	}
	
	tab->tableauINode[tab->nbElement]->numBerInode = iNode;
	
	char * copyPath = malloc(strlen(pathInode)+1);
	strcpy(copyPath, pathInode);
	
	tab->tableauINode[tab->nbElement]->path = copyPath;
	tab->nbElement = tab->nbElement+1;
}


/*
* Trouver si le iNode du fichier se trouve dans le tableau et retourne son path.
* 
* @param iNode L'id du fichier a trouver dans le tableau.
* @param tab Le tableau qui contient tout les ficher hardLink archivier (ceux qui on plus de 2).
*
* @return Retourne le chemin de ce fichier present dans le tableau.
*/
char * trouverPathAvecInode (long int iNode, struct tableDeH *tab){
	char * trouver = NULL;
	
	int i;
	for(i = 0; i < tab->nbElement && trouver == NULL; i++){
		
		if(tab->tableauINode[i]->numBerInode == iNode){
			trouver = tab->tableauINode[i]->path;
		}
		
	}
	//printf("nbElement : %d\n", tab->nbElement);
	return trouver;
	
}

/*
* Analyse les deux path et creer un relative path du premier vers le deuxieme
* 
* @param depart ou le chemin relatif commence.
* @param arriver ou le chemin relatif doit arriver.
*
* @return Retourne une chaine contenant le chemin relatif de depart vers arriver
*/
char * pathRelative (char * depart, char * arriver){
	//printf("dep : %s\n", depart);
	//printf("fin : %s\n", arriver);
	int sizeD = strlen(depart);
	int sizeF = strlen(arriver);
	int i = 0;
	int j = 0;
	unsigned int x = 0;
	int minI = sizeF<sizeD?sizeF:sizeD;
	int compteur = 0;
	
	char *retour = malloc(strlen(depart)+ strlen(arriver) + 2);
	
	//position i au repertoire qui sont different
	while(i<minI && depart[i] == arriver[i]){
		i++;
		
		if(depart[i] == '/'){
			compteur = 0;
		}else{
			compteur++;
		}
		
	}
	i = i - compteur + 1;
	j = i;
	
	//calcule le nombre de repertoire different
	while(j < sizeD-1){
		if(depart[j] == '/'){
			retour[x] = '.';
			retour[x+1] = '.';
			retour[x+2] = '/';
			x = x+3;
		}
		j++;
	}
	
	
	//copie le reste du path
	while (i < sizeF){
		retour[x] = arriver[i];
		x++;
		i++;
	}
	
	while (x < strlen(retour)){
		retour[x] = '\0';
		x++;
	}
		
	return retour;
	
}


/*
* Determine la protection du ficher (777, 700, ..)
* 
* @param statistiqueFichier contient les information du fichier.
*
* @return Retourne une chaine contenant 3 octet de la protection du fichier
*/
char* protectionDuFichier(struct stat statistiqueFichier){

	char* result = malloc(4*sizeof(char));
	char tmpChar[2];

	int tmp = 0;
	
	if((statistiqueFichier.st_mode & S_IRUSR)){
		tmp += 4;
	}
	if((statistiqueFichier.st_mode & S_IWUSR)){
		tmp += 2;
	}
	if((statistiqueFichier.st_mode & S_IXUSR)){
		tmp += 1;
	}
	
	sprintf(tmpChar, "%d", tmp);
	result[0] = tmpChar[0];
	tmp = 0;
	
	if((statistiqueFichier.st_mode & S_IRGRP)){
		tmp += 4;
	}
	if((statistiqueFichier.st_mode & S_IWGRP)){
		tmp += 2;
	}
	if((statistiqueFichier.st_mode & S_IXGRP)){
		tmp += 1;
	}
	
	sprintf(tmpChar, "%d", tmp);
	result[1] = tmpChar[0];
	tmp = 0;
	

	if((statistiqueFichier.st_mode & S_IROTH)){
		tmp += 4;
	}
	if((statistiqueFichier.st_mode & S_IWOTH)){
		tmp += 2;
	}
	if((statistiqueFichier.st_mode & S_IXOTH)){
		tmp += 1;
	}
	
	sprintf(tmpChar, "%d", tmp);
	result[2] = tmpChar[0];
	result[3] = '\0';

	return result;
}

/*
* Permet de concatener les deux string passer en parametre
* 
*
* @param chemin Le chemin qui doit se faire concatener par le fichier.
* @param fichier Le fichier qui doit etre concatener.
*
* @return Retourne la concatenation des deux string en s'assaurant de former un chemin.
*/
char* concatenerRepertoire(const char *chemin, const char *fichier){
	
	int addespace = 1;
	unsigned int longueurChemin = strlen(chemin);
	
	//verifie si le slash est present avant concaterner
	if(chemin[longueurChemin-1] != '/'){
		addespace++;
	}
	
	char *retour = malloc(strlen(chemin)+ strlen(fichier) + 2);
	
	//ajouter chemin + '/' (si besoin) + fichier + '\0'
	unsigned int c = 0;
	unsigned int i = 0;
	while (c < longueurChemin) {
		retour[c] = chemin[c];
		c++;
	}

	if(addespace == 2){
		retour[c] = '/';
		c++;
	}
	
	while (i < strlen(fichier)) {
		retour[c] = fichier[i];
		c++;
		i++;
	}
	
	//s'assure que la fin soit bien definie 
	while (c < strlen(retour)) {
		retour[c] = '\0';
		c++;
	}
	
	return retour;
}

/*
* Ecris le type, securite, namel, name du fichier courant
*
* @param name Le nom du fichier.
* @param protection Un string qui represente la securite.
* @param type Un string qui represente le type.
* @param fichierArrr Le fichier ou l'information est ecrit.
* @param endroit Le chemin relatif d'ou on est situe.
*
* @return Retourne le chemin absolu du fichier a partir du debut de la lecture ou "-" si erreur.
*/
char * afficherInformation (char * name, char * protection, char* type, int fichierArrr, char* endroit){
	char * recepteur = malloc(16); 
	write(fichierArrr, type, strlen(type));

	if(protection){
		if(ecrire(fichierArrr, protection) == -1){
			free(recepteur);
			return "-";
		}
	}
	
	if(ecrire(fichierArrr, " ") == -1){
		free(recepteur);
		return "-";
	}
	
	char* nomDuFichierTmp;
	bool doitEtreLiberer = false;
	if(strcmp(endroit,"") != 0){
		nomDuFichierTmp = concatenerRepertoire(endroit, name);
		doitEtreLiberer = true;				
	}else{
		nomDuFichierTmp = name;
	}
	
	recepteur = convertIntAString(strlen(nomDuFichierTmp), recepteur);
	if(ecrire(fichierArrr, recepteur) == -1){
		free(recepteur);
		if(doitEtreLiberer){
			free(nomDuFichierTmp);
		}
		return "-";
	}
	
	if(ecrire(fichierArrr, " ") == -1){
		free(recepteur);
		if(doitEtreLiberer){
			free(nomDuFichierTmp);
		}
		return "-";
	}
	
	if(ecrire(fichierArrr, nomDuFichierTmp) == -1){
		free(recepteur);
		if(doitEtreLiberer){
			free(nomDuFichierTmp);
		}
		return "-";
	}

	free(recepteur);
	if(doitEtreLiberer){
		return nomDuFichierTmp;
	}
	
	return NULL;
	
}


/*
* Ecris les information d'un fichier normal
*
* @param fichierArrr Le fichier ou l'information est ecrit.
* @param name Le nom du fichier.
* @param endroit Le chemin relatif d'ou on est situe.
* @param statistiqueFichier Contient tout les information relatif au fichier a etudier.
* @param dossierTmp Le path absolu du fichier.
*
*/
int afficherFichierNormal (int fichierArrr, char * name, char* endroit, struct stat statistiqueFichier, char * dossierTmp){
	
	char * caractTmp = malloc(2);
	char * recepteur = malloc(16); 
	int lectureTmp = -1;
	char* securite = protectionDuFichier(statistiqueFichier);			
	char * nomDuFichierTmp = afficherInformation(name,securite, "f", fichierArrr, endroit);
	
	if(nomDuFichierTmp && nomDuFichierTmp[0] == '-'){
		free(caractTmp);
		free(recepteur);
		free(securite);
		return -1;
	}
	

	
	if(ecrire(fichierArrr, " ") == -1){
		free(caractTmp);
		free(recepteur);
		free(securite);
		return -1;
	}

	recepteur = convertIntAString(statistiqueFichier.st_size, recepteur);
	if(ecrire(fichierArrr, recepteur) == -1){
		free(caractTmp);
		free(recepteur);
		free(securite);
		return -1;
	}
	
	if(ecrire(fichierArrr, " ") == -1){
		free(caractTmp);
		free(recepteur);
		free(securite);
		return -1;
	}
				
	lectureTmp = open(dossierTmp, O_RDONLY);
	
	if(lectureTmp == -1){
		printf("Le fichier %s n'a pas pu etre lu.\n",dossierTmp);
		return -1;
	}else{
		
		int j = 0;
		for(j = 0; j < statistiqueFichier.st_size; j++){
			if(read(lectureTmp, caractTmp, 1) == -1){
				perror("Erreur lors de la lecture du fichier");
				free(caractTmp);
				free(recepteur);
				free(securite);
				close(lectureTmp);
				return -1;
			}

			if(write(fichierArrr, caractTmp, 1) == -1){
				perror("Erreur lors de l'ecriture dans le fichier");
				free(caractTmp);
				free(recepteur);
				free(securite);
				close(lectureTmp);
				return -1;
			}
			
		}

		close(lectureTmp);
	}
		
	if(ecrire(fichierArrr, "\n") == -1){
		free(caractTmp);
		free(recepteur);
		free(securite);
		return -1;
	}
	
	if(nomDuFichierTmp){
		free(nomDuFichierTmp);
	}			
	free(securite);
	free(recepteur);
	free(caractTmp);
	return 0;
}

/*
* Ecris les information d'un lien symbolique
*
* @param fichierArrr Le fichier ou l'information est ecrit.
* @param name Le nom du fichier.
* @param endroit Le chemin relatif d'ou on est situe.
* @param statistiqueFichier Contient tout les information relatif au fichier a etudier.
* @param dossierTmp Le path absolu du fichier.
*
*/
int afficherFichierSymbolique (int fichierArrr, char * name, char* endroit, struct stat statistiqueFichier, char * dossierTmp){
	
	char * recepteur = malloc(16); 
	char * nomDuFichierTmp = afficherInformation(name,NULL, "l", fichierArrr, endroit);
	
	if(nomDuFichierTmp && nomDuFichierTmp[0] == '-'){
		free(recepteur);
		return -1;
	}
	
	if(ecrire(fichierArrr, " ") == -1){
		free(recepteur);
		return -1;
	}

	//recuper le reel path.
	char * pathMax = malloc(statistiqueFichier.st_size + 1);
	if( readlink(dossierTmp, pathMax, statistiqueFichier.st_size + 1) == -1){
		perror("Erreur lors de la lecture avec readlink");
		free(pathMax);
		free(recepteur);
		return -1;
	}
	pathMax[statistiqueFichier.st_size] = '\0';
	
	//longueur du PATH relatif
	recepteur = convertIntAString(statistiqueFichier.st_size, recepteur);
	if(ecrire(fichierArrr, recepteur) == -1){
		free(pathMax);
		free(recepteur);
		return -1;
	}
	
	if(ecrire(fichierArrr, " ") == -1){
		free(pathMax);
		free(recepteur);
		return -1;
	}

	//Ecrire le PATH
	if(ecrire(fichierArrr, pathMax) == -1){
		free(pathMax);
		free(recepteur);
		return -1;
	}
	
	if(ecrire(fichierArrr, "\n") == -1){
		free(pathMax);
		free(recepteur);
		return -1;
	}

						
	free(pathMax);	
	if(nomDuFichierTmp){
		free(nomDuFichierTmp);
	}
	free(recepteur);
			
	return 0;
}

/*
* Ecris les information d'un lien dur
*
* @param fichierArrr Le fichier ou l'information est ecrit.
* @param name Le nom du fichier.
* @param endroit Le chemin relatif d'ou on est situe.
* @param cheminCeluiDejaArchiver Le chemin absolue de celui deja entre.
* @param dossierTmp Le path absolu du fichier.
*
*/
int afficherLienDur (int fichierArrr, char * name, char* endroit, char * cheminCeluiDejaArchiver, char * dossierTmp){
	char * recepteur = malloc(16); 
	char * nomDuFichierTmp = afficherInformation(name,NULL, "h", fichierArrr, endroit);	
	
	if(nomDuFichierTmp && nomDuFichierTmp[0] == '-'){
		free(recepteur);
		return -1;
	}
	
	if(ecrire(fichierArrr, " ") == -1){
		free(recepteur);
		return -1;
	}
	
	char * pathRelativeTmp = pathRelative(dossierTmp, cheminCeluiDejaArchiver);
			
	recepteur = convertIntAString(strlen(pathRelativeTmp), recepteur);

	if(ecrire(fichierArrr, recepteur) == -1){
		free(pathRelativeTmp);
		free(recepteur);
		return -1;
	}
	
	if(ecrire(fichierArrr, " ") == -1){
		free(pathRelativeTmp);
		free(recepteur);
		return -1;
	}
	
	if(ecrire(fichierArrr, pathRelativeTmp) == -1){
		free(pathRelativeTmp);
		free(recepteur);
		return -1;
	}
	
	if(ecrire(fichierArrr, "\n") == -1){
		free(pathRelativeTmp);
		free(recepteur);
		return -1;
	}
	
	free(pathRelativeTmp);
	if(nomDuFichierTmp){
		free(nomDuFichierTmp);
	}		
	free(recepteur);
	return 0;
	
}

/*
* Ecris les information d'un fichier discontinue
*
* @param fichierArrr Le fichier ou l'information est ecrit.
* @param name Le nom du fichier.
* @param endroit Le chemin relatif d'ou on est situe.
* @param statistiqueFichier Contient tout les information relatif au fichier a etudier.
* @param dossierTmp Le path absolu du fichier.
*
*/
int afficherSparseFile (int fichierArrr, char * name, char* endroit, struct stat statistiqueFichier, char * dossierTmp){
	int lectureTmp = -1;
	char * caractTmp = malloc(2);
	bool sortir = false;
	off_t offset = 0; //valeur ou debut la lecture
	ssize_t nombreCarac = 0; //valeur qui represent la position de la recherche
	ssize_t difference = 0; //La diffenrece entre nombreCarac et offset
    char * differenceS = malloc(16);  //Conteint le string d'un nombre
	
	int fd = open (dossierTmp, O_RDONLY);
	
	
	char* securite = protectionDuFichier(statistiqueFichier);			
	char * nomDuFichierTmp = afficherInformation(name,securite, "s", fichierArrr, endroit);
	if(nomDuFichierTmp && nomDuFichierTmp[0] == '-'){
		free(securite);
		free(differenceS);
		free(caractTmp);
		return -1;
	}

	if(ecrire(fichierArrr, " ") == -1){
		free(securite);
		free(differenceS);
		free(caractTmp);
		return -1;
	}
				
	do{
		//trouve le prochain DATA
		nombreCarac = lseek (fd, offset, SEEK_DATA);
		difference = nombreCarac - offset;
					
		if(nombreCarac != -1){
			

			differenceS = convertIntAString(difference, differenceS);
			if(ecrire(fichierArrr, differenceS) == -1){
				free(securite);
				free(differenceS);
				free(caractTmp);
				return -1;
			}
			if(ecrire(fichierArrr, " ") == -1){
				free(securite);
				free(differenceS);
				free(caractTmp);
				return -1;
			}
			offset = nombreCarac;
			
			//trouve le prochain trou
			nombreCarac = lseek (fd, offset, SEEK_HOLE);
			difference = nombreCarac - offset;
						
			if(nombreCarac != -1){
				close (fd);
				differenceS = convertIntAString(difference, differenceS);
				if(ecrire(fichierArrr, differenceS) == -1){
					free(securite);
					free(differenceS);
					free(caractTmp);
					return -1;
				}
				if(ecrire(fichierArrr, " ") == -1){
					free(securite);
					free(differenceS);
					free(caractTmp);
					return -1;
				}
				
				//Lit et ecrie de offset a nombreCarac.				
				lectureTmp = open(dossierTmp, O_RDONLY);
				if(lectureTmp == -1){
					printf("Le fichier %s n'a pas pu etre lu.\n",dossierTmp);
					return -1;
				}else{
					lseek(lectureTmp, offset, SEEK_SET);
					int j = 0;
					for(j = 0; j < difference; j++){
						if(read(lectureTmp, caractTmp, 1) == -1){
							perror("Erreur lors de la lecture du fichier");
							free(securite);
							free(differenceS);
							free(caractTmp);
							close(lectureTmp);
							return -1;
						}
			
						if(write(fichierArrr, caractTmp, 1) == -1){
							perror("Erreur lors de l'ecriture dans le fichier");
							free(securite);
							free(differenceS);
							free(caractTmp);
							close(lectureTmp);
							return -1;
						}

					}
					close(lectureTmp);
				}
				
				if(ecrire(fichierArrr, "\n") == -1){
					free(securite);
					free(differenceS);
					free(caractTmp);
					return -1;
				}
				offset = nombreCarac;
				
				fd = open (dossierTmp, O_RDONLY);
			}else{
				sortir = true;
			}
		}else{
			sortir = true;
		}
	}while(!sortir);
	
	//calcule le reste de trou
	differenceS = convertIntAString(statistiqueFichier.st_size-offset, differenceS);

	if(ecrire(fichierArrr, differenceS) == -1){
		free(securite);
		free(differenceS);
		free(caractTmp);
		return -1;
	}
	if(ecrire(fichierArrr, " 0 \n") == -1){
		free(securite);
		free(differenceS);
		free(caractTmp);
		return -1;
	}
		
	if(nomDuFichierTmp){
		free(nomDuFichierTmp);
	}			
	free(securite);
	free(differenceS);
	free(caractTmp);
	
	return 0;
}

/*
* Fait une lecture recursif a partire du repertoire envoyer
* 
*
* @param dossier Le path du dossier actuelle.
* @param hardLink Active l'option sur le hardLink.
* @param sparseFiles Active l'option sur le sparse files.
* @param fichierArrr Le fichier ou l'information est ecrit.
* @param endroit Le chemin relatif d'ou on est situe.
*
* @return Retourne -1 si il a un erreur, 0 si aucune option, 1 si -h, 2 si -s, 3 si les 2 options
*/
int lectureRepertoire (const char* dossier, const bool hardLink, const bool sparseFiles, int fichierArrr, char* endroit, struct tableDeH *tab){

	DIR *rep = opendir (dossier);
	struct dirent *lecture;
	struct stat statistiqueFichier;
	
	if (rep == NULL){
		perror("Erreur lors de la lecture du repertoire.");
		return -1;
	}

	while ((lecture = readdir(rep))) {
		//si dossier
		
		//verifie ces le nom du fichier est correct
		if(strcmp(lecture->d_name, ".") == 0 || 
		   strcmp(lecture->d_name, "..") == 0 || 
		   lecture->d_name[0] == '/'){
			continue;
		}
		
		char* dossierTmp = concatenerRepertoire(dossier, lecture->d_name);
		
		if(lstat(dossierTmp,&statistiqueFichier) < 0){
			printf("Erreur, un probleme est survenu lors de l'application de lstats au fichier : %s\n", dossierTmp);
			continue;
		}  
		//printf("je suis situe ici : %s\n",dossierTmp);
		
		
		if(S_ISLNK(statistiqueFichier.st_mode) || lecture->d_type == DT_LNK){
			
			if(afficherFichierSymbolique(fichierArrr, lecture->d_name, endroit, statistiqueFichier, dossierTmp) == -1){
				closedir(rep);
				return -1;
			}
			
		}else if (S_ISDIR(statistiqueFichier.st_mode)){

			char* securite = protectionDuFichier(statistiqueFichier);			
			char * nomDuFichierTmp = afficherInformation(lecture->d_name,securite, "d", fichierArrr, endroit);	
			if(nomDuFichierTmp && nomDuFichierTmp[0] == '-'){
				closedir(rep);
				return -1;
			}
			
			free(securite);

			if(ecrire(fichierArrr, "\n") == -1){
				closedir(rep);
				return -1;
			}
			
			if(lectureRepertoire(dossierTmp, hardLink, sparseFiles, fichierArrr, nomDuFichierTmp?nomDuFichierTmp:lecture->d_name, tab) == -1){
				closedir(rep);
				return -1;
			}
			
			if(nomDuFichierTmp){
				free(nomDuFichierTmp);
			}
			
		}else if(S_ISREG(statistiqueFichier.st_mode)) {
			
			//Si un hardLink H
			if(hardLink && statistiqueFichier.st_nlink>1){
			
				char * cheminCeluiDejaArchiver = trouverPathAvecInode(statistiqueFichier.st_ino, tab);

				if(cheminCeluiDejaArchiver == NULL){					
					insererINode(statistiqueFichier.st_ino, dossierTmp, tab);
					if(afficherFichierNormal(fichierArrr, lecture->d_name, endroit, statistiqueFichier, dossierTmp) == -1){
						closedir(rep);
						return -1;
					}
				}else{
					if(afficherLienDur(fichierArrr, lecture->d_name, endroit, cheminCeluiDejaArchiver, dossierTmp) == -1){
						closedir(rep);
						return -1;
					}
				}
			
			//dertiminer sparse S (regarde s'il contient des blocks vide)
			}else if(sparseFiles && (statistiqueFichier.st_blocks < statistiqueFichier.st_size / 512)){
				if(afficherSparseFile(fichierArrr, lecture->d_name, endroit, statistiqueFichier, dossierTmp) == -1){
					closedir(rep);
					return -1;
				}
			//Fichier normal
			}else{
				if(afficherFichierNormal(fichierArrr, lecture->d_name, endroit, statistiqueFichier, dossierTmp) == -1){
					closedir(rep);
					return -1;
				}
			}
			
		}else{
			printf("Erreur, ce fichier n'est pas prit en compte (ni un repertoire, fichier regulier, lien dur, lien sybolique, fichier discontinue(");
		}
		
		free(dossierTmp);
	}
			
	closedir(rep);
	return 0;
}

/*
* S'assur d'ouvrir un fichier qui a l'extension ".arrr"
*
* @param argv nomFichier le nom du fichier envoyer pas l'application.
*
* @return Retourne un le lien vers le fichier ouvert avec la bonne extension
*/
int fopenArrr (char * nomFichier){
	int fichierArrr = -1;

	//Verifie extension
	int srtlenF = strlen(nomFichier);
	bool doitAjouterExtension = srtlenF < 5;	

	if(!doitAjouterExtension){
		doitAjouterExtension = nomFichier[srtlenF-1] != 'r' || 
							   nomFichier[srtlenF-2] != 'r' || 
							   nomFichier[srtlenF-3] != 'r' || 
							   nomFichier[srtlenF-4] != 'a' || 
							   nomFichier[srtlenF-5] != '.';
	}

	if(doitAjouterExtension){
		char * tmpNomFichier = malloc(srtlenF+6);
		int i;
		for(i=0; i<srtlenF; i++){
			tmpNomFichier[i] = nomFichier[i];
		}
		tmpNomFichier[srtlenF] = '.';
		tmpNomFichier[srtlenF+1] = 'a';
		tmpNomFichier[srtlenF+2] = 'r';
		tmpNomFichier[srtlenF+3] = 'r';
		tmpNomFichier[srtlenF+4] = 'r';
		tmpNomFichier[srtlenF+5] = '\0';
		
		creat(tmpNomFichier, 0644);
		fichierArrr =  open(tmpNomFichier, O_WRONLY);
		free(tmpNomFichier);		
	}else{
		creat(nomFichier, 0644);
		fichierArrr =  open(nomFichier, O_WRONLY);
	}
	
	return fichierArrr;
	
}

/*
* Verifie les parametre passer. Il boucle sur tout les pamametre et regarde pour les cas qui 
* commence "-" et "--". Si ce n'est pas le cas un erreur est lance, sinon il verifie s'il correspond a des parametres valides.
*
* @param argv Liste qui contient les parametre.
* @param argc Le nombre d'element present dans cette liste.
*
* @return Retourne -1 si il a un erreur, 0 si aucune option, 1 si -h, 2 si -s, 3 si les 2 options
*/
int verifierParam(char *argv[],const int argc){
	int valeurDeRetour = 0;
	
	bool linkActive = false;
    bool sparseActive = false;
	bool unErreur = false;
	int i = 1;
	
	while ( i < argc-2 && !unErreur){
		char* tmp = argv[i];
		int length = strlen(tmp);
		int c = 2;
		int x = 1;
					
					
		if (tmp[0] == '-'){	
			if(tmp[1] == '-'){
				char* substring = malloc((length-1));
				while (c < length) {
					substring[c-2] = tmp[c];
					c++;
				}
							
				if(strcmp(substring,"arrrd-link") ==0 ){
					linkActive = true;
					unErreur = false;
				}else if(strcmp(substring,"jack-sparse-raw") == 0){
					sparseActive = true;
					unErreur = false;
				}else{
					unErreur = true;
				}
				free(substring);
			}else{	
				while ((x < length) && !unErreur) {
					if(tmp[x] == 'h'){
						linkActive = true;
						unErreur = false;
					}else if(tmp[x] == 's' ){
						sparseActive = true;
						unErreur = false;
					}else{
						unErreur = true;
					}
						x++;
					}
				}
			}else{
				unErreur = true;
			}
							
			if(unErreur){
				printf("Erreur : l'option %s est invalide\n", tmp);
			}	
			i++;
		}
	
	if(linkActive && !unErreur){
		valeurDeRetour = valeurDeRetour + 1;
	}
	
	if(sparseActive && !unErreur){
		valeurDeRetour = valeurDeRetour + 2;
	}
	
	if(unErreur){
		valeurDeRetour = -1;
	}
	
	return valeurDeRetour;
}


int main (int argc, char *argv[]){
    if (argc < 3){
        printf("Erreur : nombre d'arguments incorect\n");
		return 1;
    }else{
        char* path = argv[argc-1];	
		int param = verifierParam(argv, argc);
				
		if(param != -1){
			int fichierArrr = fopenArrr(argv[argc-2]);
			
			if(fichierArrr == -1){
				printf("Erreur : le fichier d'archiver n'a pas pu etre cree\n");
				return 1;
			}else{
				DIR *rep = opendir (path);
				if(rep == NULL){
					printf("Erreur : le repertoire ne peu pas etre ouvert ou est introuvable");
					close(fichierArrr);
					return 1;
				}else{
					
					if(ecrire(fichierArrr, "ARRR\n") == -1){
						closedir(rep);
						close(fichierArrr);
						return -1;
					}		
					
					// Sert a contenir les Hard link deja entrer.
					struct tableDeH *tab = NULL; 
					
					if(param == 1 || param == 3){
						int tailleTableau = 10;
						
						tab = malloc(sizeof(int)+sizeof(tailleTableau*sizeof(long int)+sizeof(char*)));
						tab->nbElementTotal = tailleTableau;
						tab->nbElement = 0;
						tab->tableauINode = malloc(tailleTableau*sizeof(long int)+sizeof(char*));
						
						int i;
						
						for(i = 0; i < tailleTableau; i++){
							tab->tableauINode[i] = malloc(sizeof(long int)+sizeof(char*));
							tab->tableauINode[i]->numBerInode = 0;						
							tab->tableauINode[i]->path = NULL;
						}

					}
					
					//lecture recursif
					int retour = lectureRepertoire(path, param == 1 || param == 3, param == 2 || param == 3, fichierArrr, "", tab);
					
					close(fichierArrr);
					
					//vide le tableau
					if(tab!= NULL){
						int i;
						for(i = 0; i < tab->nbElement; i++){
							free(tab->tableauINode[i]->path);
							free(tab->tableauINode[i]);					
						}
						free(tab);
					}
					
					if(retour == -1){
						return 1;
					}
				}
					
					
			}
		
		}
	}
	return 0;
}
